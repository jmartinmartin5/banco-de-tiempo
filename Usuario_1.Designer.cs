﻿
namespace Banco_de_Tiempo
{
    partial class Usuario_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Usuario_1));
            this.ofrecer = new System.Windows.Forms.Button();
            this.buscar = new System.Windows.Forms.Button();
            this.consultar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textUsuario = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textSaldo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataAcuerdoGenerado = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.textsocio = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataAcuerdoGenerado)).BeginInit();
            this.SuspendLayout();
            // 
            // ofrecer
            // 
            this.ofrecer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.ofrecer.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ofrecer.Location = new System.Drawing.Point(516, 526);
            this.ofrecer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ofrecer.Name = "ofrecer";
            this.ofrecer.Size = new System.Drawing.Size(152, 71);
            this.ofrecer.TabIndex = 10;
            this.ofrecer.Text = "Ofrecer";
            this.ofrecer.UseVisualStyleBackColor = false;
            this.ofrecer.Click += new System.EventHandler(this.ofrecer_Click);
            // 
            // buscar
            // 
            this.buscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.buscar.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscar.Location = new System.Drawing.Point(58, 526);
            this.buscar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(152, 71);
            this.buscar.TabIndex = 9;
            this.buscar.Text = "Monedero";
            this.buscar.UseVisualStyleBackColor = false;
            this.buscar.Click += new System.EventHandler(this.button1_Click);
            // 
            // consultar
            // 
            this.consultar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.consultar.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consultar.Location = new System.Drawing.Point(298, 526);
            this.consultar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.consultar.Name = "consultar";
            this.consultar.Size = new System.Drawing.Size(152, 71);
            this.consultar.TabIndex = 11;
            this.consultar.Text = "Consultar";
            this.consultar.UseVisualStyleBackColor = false;
            this.consultar.Click += new System.EventHandler(this.consultar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Snap ITC", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            this.label2.Location = new System.Drawing.Point(322, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(562, 73);
            this.label2.TabIndex = 12;
            this.label2.Text = "Banco de Tiempo";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textUsuario
            // 
            this.textUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.textUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textUsuario.Font = new System.Drawing.Font("Snap ITC", 26.25F, System.Drawing.FontStyle.Bold);
            this.textUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            this.textUsuario.Location = new System.Drawing.Point(358, 129);
            this.textUsuario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textUsuario.Name = "textUsuario";
            this.textUsuario.ReadOnly = true;
            this.textUsuario.Size = new System.Drawing.Size(504, 68);
            this.textUsuario.TabIndex = 13;
            this.textUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textUsuario.TextChanged += new System.EventHandler(this.nombreUsuario_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.button1.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(999, 588);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 71);
            this.button1.TabIndex = 14;
            this.button1.Text = "Salir";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // textSaldo
            // 
            this.textSaldo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.textSaldo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSaldo.Font = new System.Drawing.Font("Snap ITC", 26.25F, System.Drawing.FontStyle.Bold);
            this.textSaldo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            this.textSaldo.Location = new System.Drawing.Point(980, 115);
            this.textSaldo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textSaldo.Name = "textSaldo";
            this.textSaldo.ReadOnly = true;
            this.textSaldo.Size = new System.Drawing.Size(150, 68);
            this.textSaldo.TabIndex = 15;
            this.textSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textSaldo.TextChanged += new System.EventHandler(this.textSaldo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Snap ITC", 8.25F);
            this.label1.Location = new System.Drawing.Point(1044, 54);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 23);
            this.label1.TabIndex = 16;
            this.label1.Text = "Saldo";
            // 
            // dataAcuerdoGenerado
            // 
            this.dataAcuerdoGenerado.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.dataAcuerdoGenerado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataAcuerdoGenerado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataAcuerdoGenerado.Location = new System.Drawing.Point(190, 246);
            this.dataAcuerdoGenerado.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataAcuerdoGenerado.Name = "dataAcuerdoGenerado";
            this.dataAcuerdoGenerado.RowHeadersWidth = 62;
            this.dataAcuerdoGenerado.Size = new System.Drawing.Size(792, 245);
            this.dataAcuerdoGenerado.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(120, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.TabIndex = 22;
            this.label3.Text = "Nº Socio";
            // 
            // textsocio
            // 
            this.textsocio.Location = new System.Drawing.Point(245, 108);
            this.textsocio.Name = "textsocio";
            this.textsocio.Size = new System.Drawing.Size(100, 26);
            this.textsocio.TabIndex = 23;
            // 
            // Usuario_1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1200, 692);
            this.Controls.Add(this.textsocio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataAcuerdoGenerado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textSaldo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textUsuario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.consultar);
            this.Controls.Add(this.ofrecer);
            this.Controls.Add(this.buscar);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Usuario_1";
            this.Text = "Usuario_1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Usuario_1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataAcuerdoGenerado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ofrecer;
        private System.Windows.Forms.Button buscar;
        private System.Windows.Forms.Button consultar;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textUsuario;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textSaldo;
        private System.Windows.Forms.DataGridView dataAcuerdoGenerado;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textsocio;
    }
}