﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Banco_de_Tiempo.clases;
using Banco_de_Tiempo.models;

namespace Banco_de_Tiempo
{
    public partial class Ofrecer_servicio : Form
    {
        public String password;
        
        public Ofrecer_servicio()
        {
            InitializeComponent();
        }

        private void Ofrecer_servicio_Load(object sender, EventArgs e)
        {
            List<models.ViewModel.OfrecerServicio> tipoServicios = new List<models.ViewModel.OfrecerServicio>();
            List<clases.User> lstUsuarios = new List<User>();

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                tipoServicios = (from d in db.TipoServiciot
                                 select new models.ViewModel.OfrecerServicio
                                 {
                                     IdTipoServicio = d.idTipoServicio,
                                     Nombre = d.nombre
                                 }).ToList();
               

                combo.DataSource = db.TipoServiciot.Select(x => x.nombre).ToList();
                lstUsuarios = (from u in db.Usuariot
                               select new clases.User
                               {
                                   Id = u.idUsuario,
                                   Nombre = u.nombre
                               }).ToList();

                foreach (var a in lstUsuarios)
                {
                   
                    Usuariot us = new Usuariot();

                    us.nombre = textsocio.Text;

                    if (us.nombre == a.Nombre)
                    {

                        textId.Text = Convert.ToString(a.Id);
                    }
                }

            }
                       
        }

        private void guardar_Click(object sender, EventArgs e)
        {
            Ofertat ofertat = new Ofertat();
            int listaServiciost = (int)combo.SelectedIndex + 1;
            Usuariot usuariot = new Usuariot();
            
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                    

                            //usuariot.idUsuario == db.Usuario;

                ofertat.nombre = textsocio.Text;
                ofertat.descripcion = textdescripcion.Text;
                ofertat.fecha = dtpfecha.Value;
                ofertat.TipoServiciot_idTipoServicio = listaServiciost;
                ofertat.Usuariot_idUsuario = Convert.ToInt32(textId.Text);
                ofertat.Tiempo = Convert.ToInt32(texttiempo.Text);
              

                db.Ofertat.Add(ofertat);
                db.SaveChanges();

                this.Hide();
                Usuario_1 usr = new Usuario_1();
                
                usr.textUsuario.Text = textsocio.Text;
                usr.pass = password;
                
                usr.ShowDialog();
               // Ofrecer_servicio servicio = new Ofrecer_servicio();
               // servicio.ShowDialog();

                this.Close();
            }
        }

        private void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        
        }
        private void Tiempo_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textsocio_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                var socio = Convert.ToInt32(textId.Text);
                var u = db.Usuariot
                  .Single(b => b.idUsuario == socio);
                Usuario_1 usr = new Usuario_1();
                usr.textUsuario.Text = textsocio.Text;
                usr.textSaldo.Text = Convert.ToString(u.saldo);

                usr.ShowDialog();
            }
                
        }
    }
}


    
