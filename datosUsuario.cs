﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Banco_de_Tiempo.models;
using Banco_de_Tiempo.models.ViewModel;

namespace Banco_de_Tiempo
{
    public partial class datosUsuario : Form
    {
        public int? idUsuario;
        Usuariot usuariot = null;

        public datosUsuario(int? idUsuario = null)
        {
            InitializeComponent();

            this.idUsuario = idUsuario;
            if(idUsuario != null)
            {
                cargaDatos();
            }
        }

        private void cargaDatos()
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                // Carga los datos en el form a modificar 
                usuariot = db.Usuariot.Find(idUsuario);
                textnombre.Text = usuariot.nombre;
                textapellidos.Text = usuariot.apellidos;
               // textedad.Text = usuariot.edad;
                textdireccion.Text = usuariot.direccion;
                textemail.Text = usuariot.email;
                textpassword.Text = usuariot.password;
                textciudad.Text = usuariot.ciudad;
                textpassword.Text = usuariot.password;
                textadmin.Text = Convert.ToString(usuariot.admin);
            }
        }

        private void datosUsuario_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
       
        private void guardar_Click(object sender, EventArgs e)
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {

               
                    //Usuariot usuariot = new Usuariot();

                    usuariot.nombre = textnombre.Text;
                    usuariot.apellidos = textapellidos.Text;
                   // usuariot.edad = textedad.Text;
                    usuariot.direccion = textdireccion.Text;
                    usuariot.email = textemail.Text;
                    usuariot.password = textpassword.Text;
                    usuariot.ciudad = textciudad.Text;
                    usuariot.password = textpassword.Text;
                    //usuariot.admin = Convert.ToBoolean(textadmin.Text);
                    //usuariot.admin = textadmin.Text;




                db.Entry(usuariot).State = System.Data.Entity.EntityState.Modified;
                
               
               
                db.SaveChanges();
                this.Hide();
                datosUsuarios usuario = new datosUsuarios();
                usuario.ShowDialog();
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }
    }
}
