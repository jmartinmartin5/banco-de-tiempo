﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Banco_de_Tiempo.models;
using System.Xml.Linq;
using Banco_de_Tiempo.clases;

namespace Banco_de_Tiempo
{
    public partial class Usuario : Form
    {
        public Usuario()
        {
            InitializeComponent();
        }



        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Usuario_Load(object sender, EventArgs e)
        {


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void registrar_Click(object sender, EventArgs e)
        {
            User user = new User();

            XDocument document = new XDocument(new XDeclaration("1.0", "UTF - 8", null));
            XElement nodoRaiz = new XElement("usuarios");
            document.Add(nodoRaiz);
            XElement usuario = new XElement("usuario");
            usuario.Add(new XElement("Nombre", textnombre.Text));
            usuario.Add(new XElement("Apellidos", textapellidos.Text));
            usuario.Add(new XElement("Edad", dateTimePickerEdad.Value.Date));
            usuario.Add(new XElement("Direccion", textdireccion.Text));
            usuario.Add(new XElement("Ciudad", textciudad.Text));
            usuario.Add(new XElement("Email", textemail.Text));
            usuario.Add(new XElement("Password", textpassword.Text));
            usuario.Add(new XElement("Saldo", user.Credito()));
            usuario.Add(new XElement("Administrador", user.Administrador()));
            nodoRaiz.Add(usuario);

            document.Save(@"C:\Users\monch\Desktop\xml\usuarios.xml");
            usuariobd();
            this.Hide();
            Principal inicio = new Principal();
            inicio.ShowDialog();



        }


        private void usuariobd()
        {

            XDocument documento = XDocument.Load(@"C:\Users\monch\Desktop\xml\usuarios.xml");
            var usuarios = from usu in documento.Descendants("usuarios") select usu;
            foreach (XElement u in usuarios.Elements("usuario"))
            {
                string dateInput = u.Element("Edad").Value;
                var parsedDate = DateTime.Parse(dateInput);


                using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
                {
                    Usuariot usuariot = new Usuariot();

                    int i;
                    var ids = db.Usuariot.Select(x => new { IdUsuario = x.idUsuario });
                    foreach (var a in ids)

                    {
                        i = a.IdUsuario;
                        usuariot.idUsuario = i;

                    }


                    usuariot.nombre = u.Element("Nombre").Value;
                    usuariot.apellidos = u.Element("Apellidos").Value;
                    usuariot.edad = parsedDate;
                    usuariot.direccion = u.Element("Direccion").Value;
                    usuariot.email = u.Element("Email").Value;
                    usuariot.password = u.Element("Password").Value;
                    usuariot.ciudad = u.Element("Ciudad").Value;
                    usuariot.saldo = Convert.ToInt32(u.Element("Saldo").Value);
                    usuariot.admin = Convert.ToBoolean(u.Element("Administrador").Value);


                    db.Usuariot.Add(usuariot);

                    db.SaveChanges();

                }
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}




