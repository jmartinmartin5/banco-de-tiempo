﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Banco_de_Tiempo.models;

namespace Banco_de_Tiempo
{
    public partial class datosOferta : Form
    {
        public datosOferta()
        {
            InitializeComponent();
        }

        private void datosUsuario_Load(object sender, EventArgs e)
        {
            Refrescar();
        }

        #region Helper
        private void Refrescar()
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                var lst = db.Ofertat.Select(x => new { Nombre = x.nombre, Descripcion = x.descripcion }).ToList();

                data.DataSource = lst;
            }
        }
        #endregion

        private void data_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
