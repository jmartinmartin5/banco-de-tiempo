﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Banco_de_Tiempo.models;

namespace Banco_de_Tiempo
{
    public partial class Servicios : Form
    {
        public String password;
        public Servicios()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Servicios_Load(object sender, EventArgs e)
        {
            List<models.ViewModel.TipoServiciosViewModel> tipoServicios = new List<models.ViewModel.TipoServiciosViewModel>();
            

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                //Usuario_1 us = new Usuario_1();
                //textsocio.Text = us.textsocio.Text;
                
                tipoServicios = (from d in db.TipoServiciot
                                 select new models.ViewModel.TipoServiciosViewModel
                                 {
                                     Id = d.idTipoServicio,
                                     Nombre = d.nombre
                                 }).ToList();
                
                //combobox.DataSource = db.TipoServiciot.Select(x => x.nombre).ToList();
            }

            combobox.DataSource = tipoServicios;
            combobox.ValueMember = "Id";
            combobox.DisplayMember = "Nombre";


           
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void combobox_SelectedValueChanged(object sender, EventArgs e)
        {
            List<models.ViewModel.OfertasServicios> ofertasServicios = new List<models.ViewModel.OfertasServicios>();
            int listaServiciost = (int)combobox.SelectedIndex + 1;
            
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                
                AcuerdoGeneradot agt = new AcuerdoGeneradot();
                String n = textNombreUsuario.Text;
                var ids = db.AcuerdoGeneradot.Select(x => x.Ofertat_idServicio).ToList();
                
                ofertasServicios = (from d in db.Ofertat
                                    where d.TipoServiciot_idTipoServicio == listaServiciost && !ids.Contains(d.idServicio) && d.nombre != n



                                    select new models.ViewModel.OfertasServicios

                                    {
                                        Id = d.idServicio,
                                        Nombre = d.nombre,
                                        Descripcion = d.descripcion,
                                        Fecha = d.fecha,
                                        Socio = d.Usuariot.idUsuario,
                                        Tiempo = d.Tiempo
                                    }).ToList();
               // DatosOfertat();
                //combobox.DataSource = db.TipoServiciot.Select(x => x.nombre).ToList();
            }

          



            data.DataSource = ofertasServicios;

        }

        private void DatosOfertat()
        {

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                var lst = db.Ofertat.Select(x => new {  Nombre = x.nombre, Descripcion = x.descripcion, /*Fecha = x.fecha,*/ Usuario = x.Usuariot.nombre }).ToList();
                data.DataSource = lst.ToList();
            }

        }


       /* private void Seleccionar()
        {
           // int idServicio = int.Parse(dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[0].Value.ToString());

           // if (idServicio != null)
           // {
             //   Servicios servicio = new Servicios(idServicio);
                
            //}
            Refrescar();

        }*/

        private void Refrescar()
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                var lst = db.Usuariot.Select(x => new { IdUsuario = x.idUsuario, Nombre = x.nombre, Apellidos = x.apellidos, Edad = x.edad, Dirección = x.direccion, Ciudad = x.ciudad, Telefono = x.telefono, Email = x.email }).ToList();
               // data.DataSource = lst.ToList();
            }
        }

        private void Seleccion_Click(object sender, EventArgs e)
        {
            string nombre = data.Rows[data.CurrentRow.Index].Cells[5].Value.ToString();

            int? id = int.Parse(data.Rows[data.CurrentRow.Index].Cells[0].Value.ToString());
            if (id != null)
            {
                this.Hide();
                Acuerdo acuerdo = new Acuerdo(id);
                acuerdo.textsocio2.Text = nombre;
                acuerdo.textsocio1.Text = textsocio.Text;
                acuerdo.textNombreUsuario.Text = textNombreUsuario.Text;     
                acuerdo.ShowDialog();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Usuario_1 usuario = new Usuario_1();
            usuario.textsocio.Text = textsocio.Text;
            usuario.textSaldo.Text = textsaldo.Text;
            usuario.textUsuario.Text = textNombreUsuario.Text;
            usuario.pass = password;
            usuario.ShowDialog();
        }

        private void textNombreUsuario_TextChanged(object sender, EventArgs e)
        {

        }
    }

    

}


