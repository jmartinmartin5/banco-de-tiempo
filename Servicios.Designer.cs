﻿
namespace Banco_de_Tiempo
{
    partial class Servicios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Servicios));
            this.label2 = new System.Windows.Forms.Label();
            this.Seleccion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.combobox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.data = new System.Windows.Forms.DataGridView();
            this.volverButton = new System.Windows.Forms.Button();
            this.textNombreUsuario = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textsocio = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textsaldo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.data)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Snap ITC", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            this.label2.Location = new System.Drawing.Point(358, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(562, 73);
            this.label2.TabIndex = 13;
            this.label2.Text = "Banco de Tiempo";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Seleccion
            // 
            this.Seleccion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.Seleccion.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seleccion.Location = new System.Drawing.Point(916, 218);
            this.Seleccion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Seleccion.Name = "Seleccion";
            this.Seleccion.Size = new System.Drawing.Size(152, 71);
            this.Seleccion.TabIndex = 16;
            this.Seleccion.Text = "Seleccionar";
            this.Seleccion.UseVisualStyleBackColor = false;
            this.Seleccion.Click += new System.EventHandler(this.Seleccion_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Snap ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(476, 285);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 31);
            this.label1.TabIndex = 18;
            this.label1.Text = "Usuarios Disponibles";
            // 
            // combobox
            // 
            this.combobox.FormattingEnabled = true;
            this.combobox.Location = new System.Drawing.Point(392, 228);
            this.combobox.Name = "combobox";
            this.combobox.Size = new System.Drawing.Size(432, 28);
            this.combobox.TabIndex = 19;
            this.combobox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.combobox.SelectedValueChanged += new System.EventHandler(this.combobox_SelectedValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Snap ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(489, 166);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(241, 31);
            this.label3.TabIndex = 14;
            this.label3.Text = "Tipo de Servicio";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // data
            // 
            this.data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.data.Location = new System.Drawing.Point(148, 331);
            this.data.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.data.Name = "data";
            this.data.RowHeadersWidth = 62;
            this.data.Size = new System.Drawing.Size(920, 312);
            this.data.TabIndex = 20;
            this.data.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            // 
            // volverButton
            // 
            this.volverButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.volverButton.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.volverButton.Location = new System.Drawing.Point(148, 218);
            this.volverButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.volverButton.Name = "volverButton";
            this.volverButton.Size = new System.Drawing.Size(152, 71);
            this.volverButton.TabIndex = 21;
            this.volverButton.Text = "Volver";
            this.volverButton.UseVisualStyleBackColor = false;
            this.volverButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // textNombreUsuario
            // 
            this.textNombreUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.textNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textNombreUsuario.Font = new System.Drawing.Font("Snap ITC", 26.25F, System.Drawing.FontStyle.Bold);
            this.textNombreUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            this.textNombreUsuario.Location = new System.Drawing.Point(352, 108);
            this.textNombreUsuario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textNombreUsuario.Name = "textNombreUsuario";
            this.textNombreUsuario.ReadOnly = true;
            this.textNombreUsuario.Size = new System.Drawing.Size(504, 68);
            this.textNombreUsuario.TabIndex = 22;
            this.textNombreUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 20);
            this.label4.TabIndex = 23;
            this.label4.Text = "Nª Socio";
            // 
            // textsocio
            // 
            this.textsocio.Location = new System.Drawing.Point(184, 108);
            this.textsocio.Name = "textsocio";
            this.textsocio.Size = new System.Drawing.Size(100, 26);
            this.textsocio.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(779, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 20);
            this.label5.TabIndex = 25;
            this.label5.Text = "Credito Disponible";
            // 
            // textsaldo
            // 
            this.textsaldo.Location = new System.Drawing.Point(1000, 113);
            this.textsaldo.Name = "textsaldo";
            this.textsaldo.Size = new System.Drawing.Size(100, 26);
            this.textsaldo.TabIndex = 26;
            // 
            // Servicios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1200, 692);
            this.Controls.Add(this.textsaldo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textsocio);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textNombreUsuario);
            this.Controls.Add(this.volverButton);
            this.Controls.Add(this.data);
            this.Controls.Add(this.combobox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Seleccion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Servicios";
            this.Text = "Servicios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Servicios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.data)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Seleccion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox combobox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView data;
        private System.Windows.Forms.Button volverButton;
        public System.Windows.Forms.TextBox textNombreUsuario;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox textsocio;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textsaldo;
    }
}