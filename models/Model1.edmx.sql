
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/30/2021 01:50:14
-- Generated from EDMX file: C:\Users\juanm\OneDrive\Escritorio\P-6_bis\banco-de-tiempo\models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Banco_de_Tiempo];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AcuerdoGeneradotDemandat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AcuerdoGeneradot] DROP CONSTRAINT [FK_AcuerdoGeneradotDemandat];
GO
IF OBJECT_ID(N'[dbo].[FK_AcuerdoGeneradotOfertat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AcuerdoGeneradot] DROP CONSTRAINT [FK_AcuerdoGeneradotOfertat];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuariotDemandat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Demandat] DROP CONSTRAINT [FK_UsuariotDemandat];
GO
IF OBJECT_ID(N'[dbo].[FK_TipoServiciotOfertat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Ofertat] DROP CONSTRAINT [FK_TipoServiciotOfertat];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuariotOfertat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Ofertat] DROP CONSTRAINT [FK_UsuariotOfertat];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuariotPagot]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Pagot] DROP CONSTRAINT [FK_UsuariotPagot];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuariotCobrot]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cobrot] DROP CONSTRAINT [FK_UsuariotCobrot];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AcuerdoGeneradot]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AcuerdoGeneradot];
GO
IF OBJECT_ID(N'[dbo].[Cobrot]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cobrot];
GO
IF OBJECT_ID(N'[dbo].[Demandat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Demandat];
GO
IF OBJECT_ID(N'[dbo].[Ofertat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Ofertat];
GO
IF OBJECT_ID(N'[dbo].[Pagot]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Pagot];
GO
IF OBJECT_ID(N'[dbo].[TipoServiciot]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipoServiciot];
GO
IF OBJECT_ID(N'[dbo].[Usuariot]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Usuariot];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AcuerdoGeneradot'
CREATE TABLE [dbo].[AcuerdoGeneradot] (
    [idAcuerdo] int IDENTITY(1,1) NOT NULL,
    [fechaInicio] datetime  NULL,
    [fechaFin] datetime  NULL,
    [Ofertat_idServicio] int  NOT NULL,
    [Demandat_Id] int  NOT NULL
);
GO

-- Creating table 'Cobrot'
CREATE TABLE [dbo].[Cobrot] (
    [idCuenta] int IDENTITY(1,1) NOT NULL,
    [cantidad] int  NULL,
    [Usuariot_idUsuario] int  NOT NULL
);
GO

-- Creating table 'Demandat'
CREATE TABLE [dbo].[Demandat] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Usuariot_idUsuario] int  NOT NULL
);
GO

-- Creating table 'Ofertat'
CREATE TABLE [dbo].[Ofertat] (
    [idServicio] int IDENTITY(1,1) NOT NULL,
    [nombre] varchar(50)  NULL,
    [descripcion] varchar(50)  NULL,
    [Tiempo] int  NOT NULL,
    [fecha] datetime  NOT NULL,
    [TipoServiciot_idTipoServicio] int  NOT NULL,
    [Usuariot_idUsuario] int  NOT NULL
);
GO

-- Creating table 'Pagot'
CREATE TABLE [dbo].[Pagot] (
    [idPago] int IDENTITY(1,1) NOT NULL,
    [cantidad] int  NOT NULL,
    [Usuariot_idUsuario] int  NOT NULL
);
GO

-- Creating table 'TipoServiciot'
CREATE TABLE [dbo].[TipoServiciot] (
    [idTipoServicio] int IDENTITY(1,1) NOT NULL,
    [nombre] varchar(50)  NULL
);
GO

-- Creating table 'Usuariot'
CREATE TABLE [dbo].[Usuariot] (
    [idUsuario] int IDENTITY(1,1) NOT NULL,
    [nombre] varchar(50)  NULL,
    [apellidos] varchar(50)  NULL,
    [edad] datetime  NULL,
    [direccion] varchar(50)  NULL,
    [ciudad] varchar(50)  NULL,
    [telefono] varchar(50)  NULL,
    [email] varchar(50)  NULL,
    [password] varchar(50)  NULL,
    [admin] bit  NULL,
    [saldo] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [idAcuerdo] in table 'AcuerdoGeneradot'
ALTER TABLE [dbo].[AcuerdoGeneradot]
ADD CONSTRAINT [PK_AcuerdoGeneradot]
    PRIMARY KEY CLUSTERED ([idAcuerdo] ASC);
GO

-- Creating primary key on [idCuenta] in table 'Cobrot'
ALTER TABLE [dbo].[Cobrot]
ADD CONSTRAINT [PK_Cobrot]
    PRIMARY KEY CLUSTERED ([idCuenta] ASC);
GO

-- Creating primary key on [Id] in table 'Demandat'
ALTER TABLE [dbo].[Demandat]
ADD CONSTRAINT [PK_Demandat]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [idServicio] in table 'Ofertat'
ALTER TABLE [dbo].[Ofertat]
ADD CONSTRAINT [PK_Ofertat]
    PRIMARY KEY CLUSTERED ([idServicio] ASC);
GO

-- Creating primary key on [idPago] in table 'Pagot'
ALTER TABLE [dbo].[Pagot]
ADD CONSTRAINT [PK_Pagot]
    PRIMARY KEY CLUSTERED ([idPago] ASC);
GO

-- Creating primary key on [idTipoServicio] in table 'TipoServiciot'
ALTER TABLE [dbo].[TipoServiciot]
ADD CONSTRAINT [PK_TipoServiciot]
    PRIMARY KEY CLUSTERED ([idTipoServicio] ASC);
GO

-- Creating primary key on [idUsuario] in table 'Usuariot'
ALTER TABLE [dbo].[Usuariot]
ADD CONSTRAINT [PK_Usuariot]
    PRIMARY KEY CLUSTERED ([idUsuario] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Demandat_Id] in table 'AcuerdoGeneradot'
ALTER TABLE [dbo].[AcuerdoGeneradot]
ADD CONSTRAINT [FK_AcuerdoGeneradotDemandat]
    FOREIGN KEY ([Demandat_Id])
    REFERENCES [dbo].[Demandat]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AcuerdoGeneradotDemandat'
CREATE INDEX [IX_FK_AcuerdoGeneradotDemandat]
ON [dbo].[AcuerdoGeneradot]
    ([Demandat_Id]);
GO

-- Creating foreign key on [Ofertat_idServicio] in table 'AcuerdoGeneradot'
ALTER TABLE [dbo].[AcuerdoGeneradot]
ADD CONSTRAINT [FK_AcuerdoGeneradotOfertat]
    FOREIGN KEY ([Ofertat_idServicio])
    REFERENCES [dbo].[Ofertat]
        ([idServicio])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AcuerdoGeneradotOfertat'
CREATE INDEX [IX_FK_AcuerdoGeneradotOfertat]
ON [dbo].[AcuerdoGeneradot]
    ([Ofertat_idServicio]);
GO

-- Creating foreign key on [Usuariot_idUsuario] in table 'Demandat'
ALTER TABLE [dbo].[Demandat]
ADD CONSTRAINT [FK_UsuariotDemandat]
    FOREIGN KEY ([Usuariot_idUsuario])
    REFERENCES [dbo].[Usuariot]
        ([idUsuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuariotDemandat'
CREATE INDEX [IX_FK_UsuariotDemandat]
ON [dbo].[Demandat]
    ([Usuariot_idUsuario]);
GO

-- Creating foreign key on [TipoServiciot_idTipoServicio] in table 'Ofertat'
ALTER TABLE [dbo].[Ofertat]
ADD CONSTRAINT [FK_TipoServiciotOfertat]
    FOREIGN KEY ([TipoServiciot_idTipoServicio])
    REFERENCES [dbo].[TipoServiciot]
        ([idTipoServicio])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoServiciotOfertat'
CREATE INDEX [IX_FK_TipoServiciotOfertat]
ON [dbo].[Ofertat]
    ([TipoServiciot_idTipoServicio]);
GO

-- Creating foreign key on [Usuariot_idUsuario] in table 'Ofertat'
ALTER TABLE [dbo].[Ofertat]
ADD CONSTRAINT [FK_UsuariotOfertat]
    FOREIGN KEY ([Usuariot_idUsuario])
    REFERENCES [dbo].[Usuariot]
        ([idUsuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuariotOfertat'
CREATE INDEX [IX_FK_UsuariotOfertat]
ON [dbo].[Ofertat]
    ([Usuariot_idUsuario]);
GO

-- Creating foreign key on [Usuariot_idUsuario] in table 'Pagot'
ALTER TABLE [dbo].[Pagot]
ADD CONSTRAINT [FK_UsuariotPagot]
    FOREIGN KEY ([Usuariot_idUsuario])
    REFERENCES [dbo].[Usuariot]
        ([idUsuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuariotPagot'
CREATE INDEX [IX_FK_UsuariotPagot]
ON [dbo].[Pagot]
    ([Usuariot_idUsuario]);
GO

-- Creating foreign key on [Usuariot_idUsuario] in table 'Cobrot'
ALTER TABLE [dbo].[Cobrot]
ADD CONSTRAINT [FK_UsuariotCobrot]
    FOREIGN KEY ([Usuariot_idUsuario])
    REFERENCES [dbo].[Usuariot]
        ([idUsuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuariotCobrot'
CREATE INDEX [IX_FK_UsuariotCobrot]
ON [dbo].[Cobrot]
    ([Usuariot_idUsuario]);
GO

INSERT INTO [dbo].[TipoServiciot]
           ([nombre])
     VALUES
         ('Informática'), 
         ('Peluquería'),
         ('Repostería'),
         ('Mecánica'),
         ('Limpieza')
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------