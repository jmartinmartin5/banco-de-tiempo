﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco_de_Tiempo.models.ViewModel
{
    class AcuerdoGenerado
    {
        public int Id { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }

        public int Oferta { get; set; }
        public int Demanda {get;set;}

    }
}
