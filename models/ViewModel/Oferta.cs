﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco_de_Tiempo.models.ViewModel
{
    class Oferta
    {
       public  int IdServicio { get; set; }

       public  String Nombre { get; set; }

       public  String Descripcion { get; set; }

       public int Tiempo { get; set; }

       public  DateTime Fecha { get; set; }

       public  int TipodeServicio_IdTipoDeServicio { get; set; }

       public int Usuario_IdUsuario { get; set; }


    }
}
