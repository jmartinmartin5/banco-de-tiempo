﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco_de_Tiempo.models.ViewModel
{
    public class OfertasServicios
    {
        public int Id { get; set; }
        public string Nombre {get;set;}
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public int Tiempo { get; set; }
       // public int IdTipoServicio { get; set; }
        public int Socio { get; set; }

    }
}
