﻿
namespace Banco_de_Tiempo
{
    partial class Usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Usuario));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textnombre = new System.Windows.Forms.TextBox();
            this.textapellidos = new System.Windows.Forms.TextBox();
            this.textciudad = new System.Windows.Forms.TextBox();
            this.textdireccion = new System.Windows.Forms.TextBox();
            this.textpassword = new System.Windows.Forms.TextBox();
            this.textemail = new System.Windows.Forms.TextBox();
            this.registrar = new System.Windows.Forms.Button();
            this.dateTimePickerEdad = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Snap ITC", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            this.label2.Location = new System.Drawing.Point(591, -3);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(255, 219);
            this.label2.TabIndex = 1;
            this.label2.Text = "Banco \r\nde \r\nTiempo";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Snap ITC", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(1119, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 90);
            this.label1.TabIndex = 2;
            this.label1.Text = "Registro\r\n de \r\nUsusario";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(74, 366);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nombre";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(74, 445);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Apellidos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(63, 518);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(211, 23);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fecha de Nacimiento";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1144, 426);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 23);
            this.label7.TabIndex = 7;
            this.label7.Text = "Dirección";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1144, 361);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 23);
            this.label8.TabIndex = 8;
            this.label8.Text = "Ciudad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1144, 500);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 23);
            this.label9.TabIndex = 9;
            this.label9.Text = "Password";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1144, 563);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 23);
            this.label10.TabIndex = 10;
            this.label10.Text = "Email";
            // 
            // textnombre
            // 
            this.textnombre.Location = new System.Drawing.Point(359, 366);
            this.textnombre.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textnombre.Name = "textnombre";
            this.textnombre.Size = new System.Drawing.Size(298, 26);
            this.textnombre.TabIndex = 11;
            this.textnombre.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textapellidos
            // 
            this.textapellidos.Location = new System.Drawing.Point(359, 445);
            this.textapellidos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textapellidos.Name = "textapellidos";
            this.textapellidos.Size = new System.Drawing.Size(296, 26);
            this.textapellidos.TabIndex = 12;
            // 
            // textciudad
            // 
            this.textciudad.Location = new System.Drawing.Point(1427, 360);
            this.textciudad.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textciudad.Name = "textciudad";
            this.textciudad.Size = new System.Drawing.Size(148, 26);
            this.textciudad.TabIndex = 13;
            // 
            // textdireccion
            // 
            this.textdireccion.Location = new System.Drawing.Point(1427, 423);
            this.textdireccion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textdireccion.Name = "textdireccion";
            this.textdireccion.Size = new System.Drawing.Size(148, 26);
            this.textdireccion.TabIndex = 14;
            // 
            // textpassword
            // 
            this.textpassword.Location = new System.Drawing.Point(1427, 497);
            this.textpassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textpassword.Name = "textpassword";
            this.textpassword.Size = new System.Drawing.Size(148, 26);
            this.textpassword.TabIndex = 15;
            // 
            // textemail
            // 
            this.textemail.Location = new System.Drawing.Point(1427, 560);
            this.textemail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textemail.Name = "textemail";
            this.textemail.Size = new System.Drawing.Size(148, 26);
            this.textemail.TabIndex = 17;
            // 
            // registrar
            // 
            this.registrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.registrar.Font = new System.Drawing.Font("Snap ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registrar.Location = new System.Drawing.Point(800, 618);
            this.registrar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.registrar.Name = "registrar";
            this.registrar.Size = new System.Drawing.Size(152, 60);
            this.registrar.TabIndex = 19;
            this.registrar.Text = "Registrar";
            this.registrar.UseVisualStyleBackColor = false;
            this.registrar.Click += new System.EventHandler(this.registrar_Click);
            // 
            // dateTimePickerEdad
            // 
            this.dateTimePickerEdad.Location = new System.Drawing.Point(359, 518);
            this.dateTimePickerEdad.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateTimePickerEdad.Name = "dateTimePickerEdad";
            this.dateTimePickerEdad.Size = new System.Drawing.Size(298, 26);
            this.dateTimePickerEdad.TabIndex = 20;
            // 
            // Usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(243)))), ((int)(((byte)(212)))));
            this.ClientSize = new System.Drawing.Size(1820, 692);
            this.Controls.Add(this.dateTimePickerEdad);
            this.Controls.Add(this.registrar);
            this.Controls.Add(this.textemail);
            this.Controls.Add(this.textpassword);
            this.Controls.Add(this.textdireccion);
            this.Controls.Add(this.textciudad);
            this.Controls.Add(this.textapellidos);
            this.Controls.Add(this.textnombre);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Usuario";
            this.Text = "Usuario";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Usuario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textnombre;
        private System.Windows.Forms.TextBox textapellidos;
        private System.Windows.Forms.TextBox textciudad;
        private System.Windows.Forms.TextBox textdireccion;
        private System.Windows.Forms.TextBox textpassword;
        private System.Windows.Forms.TextBox textemail;
        private System.Windows.Forms.Button registrar;
        private System.Windows.Forms.DateTimePicker dateTimePickerEdad;
    }
}