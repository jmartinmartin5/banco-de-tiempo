﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Banco_de_Tiempo.clases;
using Banco_de_Tiempo.models;

namespace Banco_de_Tiempo
{

    public partial class Usuario_1 : Form
    {
        public String pass;

        public Usuario_1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Monedero m = new Monedero();
            m.ShowDialog();
        }

        private void Usuario_1_Load(object sender, EventArgs e)

        {
            List<models.ViewModel.AcuerdoGenerado> acuerdoGenerados = new List<models.ViewModel.AcuerdoGenerado>();

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {

                Demandat demanda = new Demandat();
                Usuario us = new Usuario();
                
                String n = textUsuario.Text;
                
                var socio = db.Usuariot
                  .Single(b => b.nombre == n );
                var id = socio.idUsuario;
                textsocio.Text = Convert.ToString(id);
                var de = (from d in db.Demandat
                          where d.Usuariot_idUsuario == id
                          select d.Id).ToList();


                acuerdoGenerados = (from d in db.AcuerdoGeneradot
                                    where de.Contains(d.Demandat_Id)

                                    select new models.ViewModel.AcuerdoGenerado

                                    {
                                        Id = d.idAcuerdo,
                                        FechaInicio = (DateTime)d.fechaInicio,
                                        FechaFin = (DateTime)d.fechaFin,
                                        Oferta = d.Ofertat_idServicio,
                                        Demanda = d.Demandat_Id,

                                    }).ToList();
            }



            dataAcuerdoGenerado.DataSource = acuerdoGenerados;
            saldo();
            //sumar();

        }

        private void consultar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Servicios servicio = new Servicios();
            servicio.textsaldo.Text = textSaldo.Text;
            servicio.textsocio.Text = textsocio.Text;
            servicio.textNombreUsuario.Text = textUsuario.Text;
            servicio.password = pass;
            servicio.ShowDialog();

        }

        private void ofrecer_Click(object sender, EventArgs e)
        {
            this.Hide();
            Ofrecer_servicio servicio = new Ofrecer_servicio();
            servicio.password = pass;

          
            servicio.textsocio.Text = textUsuario.Text;
            servicio.ShowDialog();
        }

        private void nombreUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }


        public void saldo()
        {
            List<clases.User> lstUsuarios = new List<User>();

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                lstUsuarios = (from u in db.Usuariot
                               select new clases.User
                               {
                                   Id = u.idUsuario,
                                   Nombre = u.nombre,
                                   Password = u.password,
                                   Saldo = (int)u.saldo

                               }).ToList();

                foreach (var a in lstUsuarios)
                {

                    Usuariot us = new Usuariot();

                    String name;
                    us.nombre = textUsuario.Text;
                    name = us.nombre;



                    if ((pass == a.Password) & (name == a.Nombre))
                    {

                        textSaldo.Text = Convert.ToString(a.Saldo);
                    }
                }

            }
        }

        private void textSaldo_TextChanged(object sender, EventArgs e)
        {

        }
       
    }
}
