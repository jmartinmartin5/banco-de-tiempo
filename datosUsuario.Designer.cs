﻿namespace Banco_de_Tiempo
{
    partial class datosUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textnombre = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textapellidos = new System.Windows.Forms.TextBox();
            this.textedad = new System.Windows.Forms.TextBox();
            this.textciudad = new System.Windows.Forms.TextBox();
            this.textdireccion = new System.Windows.Forms.TextBox();
            this.texttelefono = new System.Windows.Forms.TextBox();
            this.textemail = new System.Windows.Forms.TextBox();
            this.textpassword = new System.Windows.Forms.TextBox();
            this.textadmin = new System.Windows.Forms.TextBox();
            this.guardar = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellidos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Edad";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Dirección";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(70, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ciudad";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(70, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Telefono";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(70, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 20);
            this.label7.TabIndex = 6;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(70, 306);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Email";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(70, 351);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Password";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(70, 397);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Admin";
            // 
            // textnombre
            // 
            this.textnombre.Location = new System.Drawing.Point(236, 35);
            this.textnombre.Name = "textnombre";
            this.textnombre.Size = new System.Drawing.Size(373, 26);
            this.textnombre.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(316, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 20);
            this.label11.TabIndex = 11;
            // 
            // textapellidos
            // 
            this.textapellidos.Location = new System.Drawing.Point(236, 83);
            this.textapellidos.Name = "textapellidos";
            this.textapellidos.Size = new System.Drawing.Size(373, 26);
            this.textapellidos.TabIndex = 12;
            // 
            // textedad
            // 
            this.textedad.Location = new System.Drawing.Point(236, 122);
            this.textedad.Name = "textedad";
            this.textedad.Size = new System.Drawing.Size(373, 26);
            this.textedad.TabIndex = 13;
            // 
            // textciudad
            // 
            this.textciudad.Location = new System.Drawing.Point(236, 171);
            this.textciudad.Name = "textciudad";
            this.textciudad.Size = new System.Drawing.Size(373, 26);
            this.textciudad.TabIndex = 14;
            // 
            // textdireccion
            // 
            this.textdireccion.Location = new System.Drawing.Point(236, 212);
            this.textdireccion.Name = "textdireccion";
            this.textdireccion.Size = new System.Drawing.Size(373, 26);
            this.textdireccion.TabIndex = 15;
            // 
            // texttelefono
            // 
            this.texttelefono.Location = new System.Drawing.Point(236, 258);
            this.texttelefono.Name = "texttelefono";
            this.texttelefono.Size = new System.Drawing.Size(373, 26);
            this.texttelefono.TabIndex = 16;
            // 
            // textemail
            // 
            this.textemail.Location = new System.Drawing.Point(236, 306);
            this.textemail.Name = "textemail";
            this.textemail.Size = new System.Drawing.Size(373, 26);
            this.textemail.TabIndex = 17;
            // 
            // textpassword
            // 
            this.textpassword.Location = new System.Drawing.Point(236, 351);
            this.textpassword.Name = "textpassword";
            this.textpassword.Size = new System.Drawing.Size(373, 26);
            this.textpassword.TabIndex = 18;
            // 
            // textadmin
            // 
            this.textadmin.Location = new System.Drawing.Point(236, 391);
            this.textadmin.Name = "textadmin";
            this.textadmin.Size = new System.Drawing.Size(373, 26);
            this.textadmin.TabIndex = 19;
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(657, 354);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(105, 63);
            this.guardar.TabIndex = 20;
            this.guardar.Text = "Guardar";
            this.guardar.UseVisualStyleBackColor = true;
            this.guardar.Click += new System.EventHandler(this.guardar_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // datosUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 449);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.textadmin);
            this.Controls.Add(this.textpassword);
            this.Controls.Add(this.textemail);
            this.Controls.Add(this.texttelefono);
            this.Controls.Add(this.textdireccion);
            this.Controls.Add(this.textciudad);
            this.Controls.Add(this.textedad);
            this.Controls.Add(this.textapellidos);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textnombre);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "datosUsuario";
            this.Text = "Datos Usuario";
            this.Load += new System.EventHandler(this.datosUsuario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textnombre;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textapellidos;
        private System.Windows.Forms.TextBox textedad;
        private System.Windows.Forms.TextBox textciudad;
        private System.Windows.Forms.TextBox textdireccion;
        private System.Windows.Forms.TextBox texttelefono;
        private System.Windows.Forms.TextBox textemail;
        private System.Windows.Forms.TextBox textpassword;
        private System.Windows.Forms.TextBox textadmin;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}