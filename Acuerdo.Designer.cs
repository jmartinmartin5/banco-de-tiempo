﻿namespace Banco_de_Tiempo
{
    partial class Acuerdo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.textNombreUsuario = new System.Windows.Forms.TextBox();
            this.buttonRegresar = new System.Windows.Forms.Button();
            this.textFecha = new System.Windows.Forms.TextBox();
            this.buttonComprar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textDescripcion = new System.Windows.Forms.TextBox();
            this.textTipo = new System.Windows.Forms.TextBox();
            this.textCredito = new System.Windows.Forms.TextBox();
            this.textsocio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textsocio1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textsocio2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.splitContainer1.Panel1.Controls.Add(this.textsocio1);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.textNombreUsuario);
            this.splitContainer1.Panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.splitContainer1.Panel2.Controls.Add(this.textsocio2);
            this.splitContainer1.Panel2.Controls.Add(this.label7);
            this.splitContainer1.Panel2.Controls.Add(this.buttonRegresar);
            this.splitContainer1.Panel2.Controls.Add(this.textFecha);
            this.splitContainer1.Panel2.Controls.Add(this.buttonComprar);
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Panel2.Controls.Add(this.textDescripcion);
            this.splitContainer1.Panel2.Controls.Add(this.textTipo);
            this.splitContainer1.Panel2.Controls.Add(this.textCredito);
            this.splitContainer1.Panel2.Controls.Add(this.textsocio);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(800, 449);
            this.splitContainer1.SplitterDistance = 129;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // textNombreUsuario
            // 
            this.textNombreUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.textNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textNombreUsuario.Font = new System.Drawing.Font("Snap ITC", 26.25F, System.Drawing.FontStyle.Bold);
            this.textNombreUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(5)))), ((int)(((byte)(73)))));
            this.textNombreUsuario.Location = new System.Drawing.Point(266, 37);
            this.textNombreUsuario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textNombreUsuario.Name = "textNombreUsuario";
            this.textNombreUsuario.ReadOnly = true;
            this.textNombreUsuario.Size = new System.Drawing.Size(308, 68);
            this.textNombreUsuario.TabIndex = 0;
            this.textNombreUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textNombreUsuario.TextChanged += new System.EventHandler(this.textNombreUsuario_TextChanged);
            // 
            // buttonRegresar
            // 
            this.buttonRegresar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.buttonRegresar.Font = new System.Drawing.Font("Snap ITC", 8.25F);
            this.buttonRegresar.Location = new System.Drawing.Point(639, 198);
            this.buttonRegresar.Name = "buttonRegresar";
            this.buttonRegresar.Size = new System.Drawing.Size(120, 52);
            this.buttonRegresar.TabIndex = 1;
            this.buttonRegresar.Text = "Regresar";
            this.buttonRegresar.UseVisualStyleBackColor = false;
            this.buttonRegresar.Click += new System.EventHandler(this.buttonRegresar_Click);
            // 
            // textFecha
            // 
            this.textFecha.Location = new System.Drawing.Point(254, 262);
            this.textFecha.Name = "textFecha";
            this.textFecha.Size = new System.Drawing.Size(324, 26);
            this.textFecha.TabIndex = 10;
            // 
            // buttonComprar
            // 
            this.buttonComprar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.buttonComprar.Font = new System.Drawing.Font("Snap ITC", 8.25F);
            this.buttonComprar.Location = new System.Drawing.Point(639, 42);
            this.buttonComprar.Name = "buttonComprar";
            this.buttonComprar.Size = new System.Drawing.Size(120, 52);
            this.buttonComprar.TabIndex = 0;
            this.buttonComprar.Text = "Comprar";
            this.buttonComprar.UseVisualStyleBackColor = false;
            this.buttonComprar.Click += new System.EventHandler(this.buttonComprar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(66, 262);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Fecha";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // textDescripcion
            // 
            this.textDescripcion.Location = new System.Drawing.Point(254, 212);
            this.textDescripcion.Name = "textDescripcion";
            this.textDescripcion.Size = new System.Drawing.Size(324, 26);
            this.textDescripcion.TabIndex = 8;
            // 
            // textTipo
            // 
            this.textTipo.Location = new System.Drawing.Point(254, 164);
            this.textTipo.Name = "textTipo";
            this.textTipo.Size = new System.Drawing.Size(324, 26);
            this.textTipo.TabIndex = 7;
            // 
            // textCredito
            // 
            this.textCredito.Location = new System.Drawing.Point(254, 118);
            this.textCredito.Name = "textCredito";
            this.textCredito.Size = new System.Drawing.Size(324, 26);
            this.textCredito.TabIndex = 6;
            // 
            // textsocio
            // 
            this.textsocio.Location = new System.Drawing.Point(254, 68);
            this.textsocio.Name = "textsocio";
            this.textsocio.Size = new System.Drawing.Size(324, 26);
            this.textsocio.TabIndex = 5;
            this.textsocio.TextChanged += new System.EventHandler(this.textsocio_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Descripción";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tipo Servicio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Credito necesario";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre Socio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "Nº socio";
            // 
            // textsocio1
            // 
            this.textsocio1.Location = new System.Drawing.Point(100, 49);
            this.textsocio1.Name = "textsocio1";
            this.textsocio1.Size = new System.Drawing.Size(100, 26);
            this.textsocio1.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(66, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 20);
            this.label7.TabIndex = 11;
            this.label7.Text = "Nª Socio";
            // 
            // textsocio2
            // 
            this.textsocio2.Location = new System.Drawing.Point(254, 25);
            this.textsocio2.Name = "textsocio2";
            this.textsocio2.Size = new System.Drawing.Size(324, 26);
            this.textsocio2.TabIndex = 12;
            // 
            // Acuerdo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 449);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Acuerdo";
            this.Text = "Realizar Compra";
            this.Load += new System.EventHandler(this.Acuerdo_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button buttonRegresar;
        private System.Windows.Forms.Button buttonComprar;
        private System.Windows.Forms.TextBox textTipo;
        private System.Windows.Forms.TextBox textsocio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textDescripcion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textFecha;
        public System.Windows.Forms.TextBox textNombreUsuario;
        public System.Windows.Forms.TextBox textCredito;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox textsocio1;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox textsocio2;
    }
}