﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Serialization;
using Banco_de_Tiempo.clases;
using Banco_de_Tiempo.models;

namespace Banco_de_Tiempo
{
    public partial class datosUsuarios : Form
    {


        public datosUsuarios()
        {
            InitializeComponent();

        }
        private void Refrescar() {

            List<User> listaUsuarios = new List<User>();

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
                {
                    var lst = db.Usuariot.Select(x => new { IdUsuario = x.idUsuario, Nombre = x.nombre, Apellidos = x.apellidos, Edad = x.edad, Dirección = x.direccion, Ciudad = x.ciudad, Telefono = x.telefono, Email = x.email }).ToList();
                    data.DataSource = lst.ToList();               
                }

            XDocument lstUsuarios = new XDocument(

                new XDeclaration("1.0", "utf-8", null),
                new XElement("Usuarios",
                from u in User.usuarios()
                select new XElement("Usuario", /*new XAttribute("Id", u.Id),*/
                new XElement("Nombre", u.Nombre),
                new XElement("Apellidos", u.Apellidos),
                new XElement("Edad", u.Edad),
                new XElement("Dirección", u.Direccion),
                new XElement("Ciudad", u.Ciudad),
                new XElement("CorreoElectrónico", u.Email),
                new XElement("Password", u.Password))
                )) ;
            lstUsuarios.Save(@"C:\Users\juanm\OneDrive\Escritorio\P-6_bis\banco-de-tiempo\XML\usuarios.xml");

        }



        private void Form1_Load(object sender, EventArgs e)
        {

            {
                Refrescar();
            }

        }

        private void data_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }


        private void modifica_Click(object sender, EventArgs e)
        {
            int? idUsuario = int.Parse(data.Rows[data.CurrentRow.Index].Cells[0].Value.ToString());

            if (idUsuario != null)
            {
                this.Hide();
                datosUsuario usuario = new datosUsuario(idUsuario);
                usuario.ShowDialog();
            }
            Refrescar();

        }

        private void data_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void elimina_Click(object sender, EventArgs e)
        {
            int? idUsuario = int.Parse(data.Rows[data.CurrentRow.Index].Cells[0].Value.ToString());

            if (idUsuario != null)
            {
                using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
                {
                    Usuariot usuariot = db.Usuariot.Find(idUsuario);
                    db.Usuariot.Remove(usuariot);
                    db.SaveChanges();
                }
                Refrescar();
            }
        }

        private void cerrar_Click(object sender, EventArgs e)
        {

            this.Hide();
            Administrador admin = new Administrador();
            admin.ShowDialog();
            //Form currentForm = Form.ActiveForm;

            ////Sierra el formulario
            //currentForm.Close();

        }
    }

  }