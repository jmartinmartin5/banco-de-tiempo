﻿using Banco_de_Tiempo.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Banco_de_Tiempo.clases
{
    [XmlRootAttribute("Usuarios", Namespace = "", IsNullable = false)]
    public class User
    {

        public User()
        {

        }



        public User(int id, string nombre, string apellidos, DateTime edad, string direccion, string ciudad, string email, int saldo,string password)
        {
            Id = id;
            Nombre = nombre;
            Apellidos = apellidos;
            Edad = edad;
            Direccion = direccion;
            Ciudad = ciudad;
            Email = email;
            Password = password;
            Saldo = saldo;
        }

        [XmlElementAttribute("Id")]
        public int Id { get; set; }

        [XmlElementAttribute("Nombre")]
        public string Nombre { get; set; }

        [XmlElementAttribute("Apellidos")]
        public string Apellidos { get; set; }

        [XmlElementAttribute("Edad")]
        public DateTime Edad { get; set; }

        [XmlElementAttribute("Direccion")]
        public string Direccion { get; set; }

        [XmlElementAttribute("Ciudad")]
        public string Ciudad { get; set; }


        [XmlElementAttribute("Email")]
        public string Email { get; set; }


        [XmlElementAttribute("Admin")]
        public bool Admin { get; set; }


        [XmlElementAttribute("Password")]
        public string Password { get; set; }

        [XmlElementAttribute("Saldo")]

        public int Saldo { get; set; }



        public static object Identity { get; internal set; }

        public void serialzarUserXML(User usuario)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(usuario.GetType());
            x.Serialize(Console.Out, usuario);
            Console.WriteLine();
            Console.ReadLine();
        }


        public static List<User> usuarios()
        {

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                return (from d in db.Usuariot
                        select new User

                        {
                            Id = d.idUsuario,
                            Nombre = d.nombre,
                            Apellidos = d.apellidos,
                            Direccion = d.direccion,
                            Ciudad = d.ciudad,
                            Edad = (DateTime)(d.edad),
                            Email = d.email,
                            Password = d.password,
                            Saldo = (int)d.saldo,
                            Admin = (bool)d.admin


                        }).ToList();

            }
        }

        public bool Administrador()
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                Usuariot usuariot = new Usuariot();
                

                int i;
                var ids = db.Usuariot.Select(x => new { IdUsuario = x.idUsuario });
                foreach (var a in ids)

                {
                    i = a.IdUsuario;
                    usuariot.idUsuario = i;

                }


                if (usuariot.idUsuario == 0)
                {
                    Admin = true;
                }
                else
                {
                    Admin = false;
                }
            }
            return Admin;
        }

        public int Credito()
        {
            Saldo = 5;
            return Saldo;
        }

        public void Pago(int cantidad)
        {
            Saldo -= cantidad;
        }


    }



}
