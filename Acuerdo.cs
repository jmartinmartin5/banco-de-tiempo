﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Banco_de_Tiempo.models;
using System.Xml.Linq;
namespace Banco_de_Tiempo
{
    public partial class Acuerdo : Form
    {
        public int? idServicio;
        Ofertat oferta = null;
        public Acuerdo(int? idServicio = null)
        {
            InitializeComponent();
            this.idServicio = idServicio;
            if (idServicio != null)
            {
                cargaDatos();
            }
        }
        private void cargaDatos()
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                AcuerdoGeneradot agt = new AcuerdoGeneradot();

                // Carga los datos en el form a modificar 
                oferta = db.Ofertat.Find(idServicio);
                textsocio.Text = Convert.ToString(oferta.Usuariot_idUsuario);
                textCredito.Text = Convert.ToString(oferta.Tiempo);
                textTipo.Text = Convert.ToString(oferta.TipoServiciot_idTipoServicio);
                textDescripcion.Text = oferta.descripcion;
                textFecha.Text = oferta.fecha.ToShortDateString().ToString();

            }
        }
        private void Acuerdo_Load(object sender, EventArgs e)
        {
            using (var context = new Banco_de_TiempoEntities())
            {
                var nombre = Convert.ToInt32(textsocio.Text);
                var n = context.Usuariot
                     .Single(b => b.idUsuario == nombre);
                var tipo = Convert.ToInt32(textTipo.Text);
                var t = context.TipoServiciot
                    .Single(b => b.idTipoServicio == tipo);

                textsocio.Text = n.nombre;
                textTipo.Text = t.nombre;



            }

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void buttonRegresar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Servicios servicio = new Servicios();
            servicio.textsocio.Text = textsocio1.Text;
            servicio.textNombreUsuario.Text = textNombreUsuario.Text;
            servicio.ShowDialog();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void acuerdo(int demanda)
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                

                AcuerdoGeneradot acuerdo = new AcuerdoGeneradot();
                Ofertat oferta = new Ofertat();
                int demandas = demanda;
                String descripcion = textDescripcion.Text;
                var socio = Convert.ToInt32(textsocio2.Text);
            
                oferta = db.Ofertat.Find(idServicio);
                var nt = db.Ofertat
                    .Single(b => b.Usuariot_idUsuario == socio && b.descripcion == descripcion );

                acuerdo.Ofertat_idServicio = nt.idServicio;



                acuerdo.Demandat_Id = demandas;


                acuerdo.fechaInicio = Convert.ToDateTime(textFecha.Text);
                acuerdo.fechaFin = Convert.ToDateTime(textFecha.Text).AddDays(7);


                db.AcuerdoGeneradot.Add(acuerdo);

                db.SaveChanges();


            }


        }
        private void buttonComprar_Click(object sender, EventArgs e)
        {

            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {

                demanda();
                pago();
               


            }

            this.Hide();
            Servicios servicio = new Servicios();
            servicio.textNombreUsuario.Text = textNombreUsuario.Text;
            servicio.ShowDialog();


        }

        private void textsocio_TextChanged(object sender, EventArgs e)
        {

        }

        private void textNombreUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void demanda()
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {

                Usuariot us = new Usuariot();

                Demandat demanda = new Demandat();
                String nombre = textNombreUsuario.Text;

                var nt = db.Usuariot
                    .Single(b => b.nombre == nombre );

                demanda.Usuariot_idUsuario = nt.idUsuario;
                db.Demandat.Add(demanda);




                db.SaveChanges();
                acuerdo(nt.idUsuario);
            }

        }
        private void pago()
        {
            using (Banco_de_TiempoEntities db = new Banco_de_TiempoEntities())
            {
                Usuariot us = new Usuariot();
                var saldo = Convert.ToInt32(textCredito.Text);
                var socioPaga = Convert.ToInt32(textsocio1.Text);
                var socioCobra = Convert.ToInt32(textsocio2.Text);


                var sp = db.Usuariot
                    .Single(b => b.idUsuario == socioPaga);

                var sc = db.Usuariot
                   .Single(b => b.idUsuario == socioCobra);

                sp.saldo = sp.saldo - saldo;
                sc.saldo = sc.saldo + saldo;

                Servicios s = new Servicios();
                s.textsaldo.Text = Convert.ToString(sp.saldo);



                db.SaveChanges();
               
            }

        }
    }
}

