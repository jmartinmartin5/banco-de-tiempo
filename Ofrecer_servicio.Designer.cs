﻿namespace Banco_de_Tiempo
{
    partial class Ofrecer_servicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.combo = new System.Windows.Forms.ComboBox();
            this.textdescripcion = new System.Windows.Forms.TextBox();
            this.textsocio = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.guardar = new System.Windows.Forms.Button();
            this.dtpfecha = new System.Windows.Forms.DateTimePicker();
            this.Tiempo = new System.Windows.Forms.Label();
            this.texttiempo = new System.Windows.Forms.TextBox();
            this.textId = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(204, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(200, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descripción";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 295);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fecha";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Id Socio";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // combo
            // 
            this.combo.FormattingEnabled = true;
            this.combo.Location = new System.Drawing.Point(472, 234);
            this.combo.Name = "combo";
            this.combo.Size = new System.Drawing.Size(274, 28);
            this.combo.TabIndex = 4;
            this.combo.SelectedIndexChanged += new System.EventHandler(this.combo_SelectedIndexChanged);
            // 
            // textdescripcion
            // 
            this.textdescripcion.Location = new System.Drawing.Point(472, 185);
            this.textdescripcion.Name = "textdescripcion";
            this.textdescripcion.Size = new System.Drawing.Size(274, 26);
            this.textdescripcion.TabIndex = 6;
            // 
            // textsocio
            // 
            this.textsocio.BackColor = System.Drawing.SystemColors.Window;
            this.textsocio.Location = new System.Drawing.Point(472, 118);
            this.textsocio.Name = "textsocio";
            this.textsocio.ReadOnly = true;
            this.textsocio.Size = new System.Drawing.Size(272, 26);
            this.textsocio.TabIndex = 8;
            this.textsocio.TextChanged += new System.EventHandler(this.textsocio_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(200, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Tipo de Servicio";
            // 
            // guardar
            // 
            this.guardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(38)))), ((int)(((byte)(233)))));
            this.guardar.Font = new System.Drawing.Font("Snap ITC", 8.25F);
            this.guardar.Location = new System.Drawing.Point(472, 458);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(274, 54);
            this.guardar.TabIndex = 10;
            this.guardar.Text = "Guardar";
            this.guardar.UseVisualStyleBackColor = false;
            this.guardar.Click += new System.EventHandler(this.guardar_Click);
            // 
            // dtpfecha
            // 
            this.dtpfecha.Location = new System.Drawing.Point(472, 289);
            this.dtpfecha.Name = "dtpfecha";
            this.dtpfecha.Size = new System.Drawing.Size(274, 26);
            this.dtpfecha.TabIndex = 11;
            // 
            // Tiempo
            // 
            this.Tiempo.AutoSize = true;
            this.Tiempo.Location = new System.Drawing.Point(200, 346);
            this.Tiempo.Name = "Tiempo";
            this.Tiempo.Size = new System.Drawing.Size(61, 20);
            this.Tiempo.TabIndex = 12;
            this.Tiempo.Text = "Tiempo";
            this.Tiempo.Click += new System.EventHandler(this.Tiempo_Click);
            // 
            // texttiempo
            // 
            this.texttiempo.Location = new System.Drawing.Point(472, 342);
            this.texttiempo.Name = "texttiempo";
            this.texttiempo.Size = new System.Drawing.Size(274, 26);
            this.texttiempo.TabIndex = 13;
            // 
            // textId
            // 
            this.textId.Location = new System.Drawing.Point(140, 14);
            this.textId.Name = "textId";
            this.textId.ReadOnly = true;
            this.textId.Size = new System.Drawing.Size(84, 26);
            this.textId.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(478, 558);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(266, 45);
            this.button1.TabIndex = 15;
            this.button1.Text = "Volver";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Ofrecer_servicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(181)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1044, 674);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textId);
            this.Controls.Add(this.texttiempo);
            this.Controls.Add(this.Tiempo);
            this.Controls.Add(this.dtpfecha);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textsocio);
            this.Controls.Add(this.textdescripcion);
            this.Controls.Add(this.combo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Ofrecer_servicio";
            this.Text = "Ofrecer Servicio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Ofrecer_servicio_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox combo;
        private System.Windows.Forms.TextBox textdescripcion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.DateTimePicker dtpfecha;
        private System.Windows.Forms.Label Tiempo;
        public System.Windows.Forms.TextBox textsocio;
        public System.Windows.Forms.TextBox textId;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox texttiempo;
    }
}